require("dotenv").config();
import express, { Request, Response, Express, NextFunction } from "express";
import connectToDatabase from "./middleware/config";

const server: Express = express();
const host = process.env.HOST;
const port = process.env.PORT;
const router = require("./router/routes");

server.use((req: Request, res: any, next: NextFunction) => {
  res.setHeader("Access-Control-Allow-Origin", process.env.CORS_ORIGIN);
  res.setHeader("Access-Control-Allow-Methods", process.env.CORS_METHOD);
  res.setHeader("Access-Control-Allow-Headers", process.env.CORS_HEADER);
  next();
});

server.use("/", connectToDatabase, router);

server.listen(port, () => {
  console.log(`This server is running in ${host}:${port}`);
});
