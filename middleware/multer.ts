import multer from "multer";
import path from "path";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, "../public/uploads")); // Set the destination folder for uploaded files
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`; // Add a unique suffix to the filename
    cb(null, `${file.fieldname}-${uniqueSuffix}-${file.originalname}`);
  },
});

const dir = path.join(__dirname, "../public/uploads");
console.log(dir);

const uploadMiddleware = multer({ storage }).array("images", 5);

export default uploadMiddleware;
