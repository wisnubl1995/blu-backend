import express, { Router } from "express";
import {
  createUser,
  getUser,
  getUserByPhoneNumber,
  updatePhotoImages,
  updateStatusAttendance,
  updateStatusMerch,
} from "../../controller/userController";
import uploadMiddleware from "../../middleware/multer";

const router: Router = express.Router();

router.get("/", getUser);
router.get("/:phoneNumber", getUserByPhoneNumber);
router.post("/upload-photos/:userId", uploadMiddleware, updatePhotoImages);
router.post("/", createUser);
router.patch("/attendance", updateStatusAttendance);
router.patch("/merch", updateStatusMerch);

module.exports = router;
