import express, { Request, Response, Router } from "express";
const path = require("path");

const router: Router = express.Router();
const userHandler = require("./handler/userHandler");

router.use(express.json());
router.use(express.static("build/public/"));

router.get("/", function (req: Request, res: Response) {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});

router.use("/user", userHandler);

module.exports = router;
