require("dotenv").config();
import nodemailer from "nodemailer";
import express, { Request, Response } from "express";
import mongoose from "mongoose";
import path from "path";
import QRCode from "qrcode";
import USER from "../model/user";
import PHOTO from "../model/photo";

const fs = require("fs");

export const createUser = async (req: Request, res: Response) => {
  const { name, email, phone } = req.body;

  try {
    if (!name || !email || !phone) {
      return res.status(500).json({ message: "Field cannot be empty" });
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      return res.status(400).json({ message: "Invalid email format" });
    }

    const existingUser = await USER.findOne({
      $or: [{ email: email }, { phone: phone }],
    });
    if (existingUser) {
      return res
        .status(409)
        .json({ message: "User with the same email or phone already exists" });
    }

    const normalizedName = name.toLowerCase().replace(/\s+/g, "-");

    let qrCodeFileName = `${normalizedName}.png`;
    let qrCodePath = path.join(__dirname, "../public/images", qrCodeFileName);
    let counter = 1;
    while (fs.existsSync(qrCodePath)) {
      qrCodeFileName = `${normalizedName}_${counter}.png`;
      qrCodePath = path.join(__dirname, "../public/images", qrCodeFileName);
      counter++;
    }

    const dir = path.dirname(qrCodePath);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    await QRCode.toFile(qrCodePath, phone);
    const domain = req.headers.host;
    const qrCodeUrl = `http://${domain}/images/${qrCodeFileName}`;

    const newUser = new USER({
      name: name,
      email: email,
      phone: phone,
      qrcodeUrl: qrCodeUrl,
    });

    await newUser.save();
    res.status(200).json(newUser);
  } catch (error) {
    res.status(500).json({ Error: error, Message: "Failed to create user" });
  } finally {
    mongoose.disconnect;
  }
};

export const getUser = async (req: Request, res: Response) => {
  try {
    const userList = await USER.find();
    res.status(200).json(userList);
  } catch (error) {
    console.log(error);
    res.status(500).json({ Error: error, Message: "Failed to fetch user" });
  } finally {
    mongoose.disconnect;
  }
};

export const getUserByPhoneNumber = async (req: Request, res: Response) => {
  const { phoneNumber } = req.params;
  if (!phoneNumber) {
    return res.status(400).json({ message: "Phone number is required" });
  }
  try {
    const user = await USER.findOne({ phone: phoneNumber });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    res.status(200).json(user);
  } catch (error) {
    res
      .status(500)
      .json({ Error: error, Message: "Failed to get user by phone number" });
  } finally {
    mongoose.disconnect;
  }
};

export const updateStatusAttendance = async (req: Request, res: Response) => {
  const { userId } = req.body;
  if (!userId) {
    return res.status(400).json({ message: "Field cannot be empty" });
  }
  try {
    const user = await USER.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    if (user.statusAttendance) {
      return res.status(200).json({ message: "You are already attended!" });
    }
    user.statusAttendance = true;
    await user.save();
    res.status(200).json(user);
  } catch (error) {
    res
      .status(500)
      .json({ Error: error, Message: "Failed to update statusAttendance" });
  } finally {
    mongoose.disconnect;
  }
};

export const updateStatusMerch = async (req: Request, res: Response) => {
  const { userId } = req.body;
  if (!userId) {
    return res.status(400).json({ message: "Field cannot be empty" });
  }
  try {
    const user = await USER.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    if (user.statusMerch) {
      return res
        .status(200)
        .json({ message: "You are already have your merch!" });
    }
    user.statusMerch = true;
    await user.save();
    res.status(200).json(user);
  } catch (error) {
    res
      .status(500)
      .json({ Error: error, Message: "Failed to update statusMerch" });
  } finally {
    mongoose.disconnect;
  }
};

export const updatePhotoImages = async (req: any, res: Response) => {
  try {
    const { userId } = req.params;
    const { files } = req;

    const user = await USER.findById(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    const photoUrls: string[] = [];

    for (const file of files) {
      const imageUrl = `../public/uploads/${file.filename}`;
      photoUrls.push(imageUrl);

      const photo = new PHOTO({
        imageUrl,
      });

      await photo.save();

      user.photoImages.push(photo._id);
    }
    await user.save();

    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: "no-reply@pk-ent.com",
        pass: "tmtzeeeedfxagcvf",
      },
    });

    const mailOptions = {
      from: "no-reply@pk-ent.com",
      to: user.email,
      subject: "Photos Uploaded",
      text: "Here are your uploaded photos",
      attachments: photoUrls.map((url) => ({
        filename: path.basename(url),
        path: path.join(__dirname, "../public", url),
      })),
    };

    await transporter.sendMail(mailOptions);

    return res
      .status(200)
      .json({ message: "Photos added to user successfully" });
  } catch (error) {
    console.error(error);
    return res
      .status(500)
      .json({ error: error, message: "Internal server error" });
  }
};
