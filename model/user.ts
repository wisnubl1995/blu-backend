import mongoose, { Schema, Document, ObjectId } from "mongoose";

interface IUser extends Document {
  name: string;
  email: string;
  phone: string;
  qrcodeUrl?: string;
  statusAttendance: boolean;
  statusMerch: boolean;
  statusPhotoWof: boolean;
  countPhotoWof: number;
  photoImages: ObjectId[];
}

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
  },
  qrcodeUrl: String,
  statusAttendance: {
    type: Boolean,
    default: false,
  },
  statusMerch: {
    type: Boolean,
    default: false,
  },
  statusPhotoWof: {
    type: Boolean,
    default: false,
  },
  countPhotoWof: {
    type: Number,
    default: 0,
  },
  photoImages: [
    {
      type: Schema.Types.ObjectId,
      ref: "Photo",
    },
  ],
});

const USER = mongoose.model<IUser>("users", userSchema);

export default USER;
