import mongoose, { Schema, Document } from "mongoose";

interface IPhoto extends Document {
  imageUrl: string;
}

const photoSchema = new Schema({
  imageUrl: {
    type: String,
    required: true,
  },
});

const PHOTO = mongoose.model<IPhoto>("Photo", photoSchema);

export default PHOTO;
